package com.lv_spring.data.rest.jpa

import io.github.perplexhub.rsql.RSQLJPAAutoConfiguration
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.context.annotation.Import

@AutoConfiguration(after = [RSQLJPAAutoConfiguration::class])
@Import(ObjectToSerializableAdvice::class)
class LvSpringDataRestJpaAutoConfiguration {
    companion object {
        val logger: Logger = LoggerFactory.getLogger(LvSpringDataRestJpaAutoConfiguration::class.java)
    }

    init {
        logger.info("Starting LvSpringDataRestJpaAutoConfiguration")
    }
}
