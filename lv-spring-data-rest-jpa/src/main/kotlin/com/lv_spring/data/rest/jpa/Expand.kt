package com.lv_spring.data.rest.jpa

class Expand(
        val expand: Map<String, Expand?>? = null,
        val select: Set<String>? = null
)
