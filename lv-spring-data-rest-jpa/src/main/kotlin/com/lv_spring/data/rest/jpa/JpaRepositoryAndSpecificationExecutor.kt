package com.lv_spring.data.rest.jpa

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor

interface JpaRepositoryAndSpecificationExecutor<T, ID> :
    JpaRepository<T, ID>,
    JpaSpecificationExecutor<T>
