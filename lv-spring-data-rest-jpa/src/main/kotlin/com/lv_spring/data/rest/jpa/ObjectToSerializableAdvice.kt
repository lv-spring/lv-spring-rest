package com.lv_spring.data.rest.jpa

import com.fasterxml.jackson.annotation.JsonIgnore
import com.lv_spring.data.rest.jpa.expand.ExpandLexer
import com.lv_spring.data.rest.jpa.expand.ExpandParser
import jakarta.persistence.EmbeddedId
import jakarta.persistence.Id
import org.antlr.v4.runtime.CharStreams
import org.antlr.v4.runtime.CommonTokenStream
import org.hibernate.collection.spi.AbstractPersistentCollection
import org.hibernate.proxy.HibernateProxy
import org.springframework.core.MethodParameter
import org.springframework.data.domain.Page
import org.springframework.http.MediaType
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.server.ServerHttpRequest
import org.springframework.http.server.ServerHttpResponse
import org.springframework.http.server.ServletServerHttpRequest
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice
import java.util.*
import kotlin.reflect.KProperty1
import kotlin.reflect.KVisibility
import kotlin.reflect.full.hasAnnotation
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.javaField


@RestControllerAdvice
class ObjectToSerializableAdvice : ResponseBodyAdvice<Any> {

    override fun supports(
        methodParameter: MethodParameter,
        converterType: Class<out HttpMessageConverter<*>>
    ): Boolean =
        AbstractRestController::class.java.isAssignableFrom(methodParameter.declaringClass)

    override fun beforeBodyWrite(
        body: Any?,
        returnType: MethodParameter,
        selectedContentType: MediaType,
        selectedConverterType: Class<out HttpMessageConverter<*>>,
        request: ServerHttpRequest,
        response: ServerHttpResponse
    ): Any? {
        val servletRequest = (request as ServletServerHttpRequest).servletRequest
        val select: Set<String>? = servletRequest.getParameter("\$select")?.split(",")?.toSet()
        val expand = servletRequest.getParameter("\$expand")?.let {
            parseExpand(it)
        }

        return when (body) {
            is List<*> -> body.map { convertToSerializable(it, select, expand) }
            is Page<*> -> mapOf(
                "totalElements" to body.totalElements,
                "pageNumber" to body.number,
                "totalPages" to body.totalPages,
                "content" to body.content.map { convertToSerializable(it, select, expand) })

            else -> convertToSerializable(body, select, expand)
        }
    }

    private fun parseExpand(input: String): Map<String, Expand?>? {
        val lexer = ExpandLexer(CharStreams.fromString(input))
        val tokens = CommonTokenStream(lexer)
        val parser = ExpandParser(tokens).expand()

        return ExpandVisitorImpl().visit(parser)
    }

    private fun convertToSerializable(
        inObj: Any?,
        select: Set<String>?,
        expand: Map<String, Expand?>?
    ): MutableMap<String, Any?>? {
        if (inObj == null) return null

        val serializableMap = mutableMapOf<String, Any?>()

        val clazz = getClazz(inObj)
        val memberProperties = getMemberProperties(clazz)

        for (prop in memberProperties) {
            val propName = prop.name
            val fieldVal = prop.get(inObj)
            if (expand?.containsKey(propName) != true && (
                    select != null && isPropertyIgnoredOrNotInSelect(prop, select)
                        || select == null
                        && (fieldVal is HibernateProxy || fieldVal is AbstractPersistentCollection<*>))
            ) continue

            if (fieldVal == null) continue

            serializableMap[propName] =
                if (isPrimitive(fieldVal)) fieldVal
                else if (fieldVal is Collection<*>)
                    if (expand?.containsKey(propName) == true)
                        fieldVal.map {
                            convertToSerializable(it, expand[propName]?.select, expand[propName]?.expand)
                        }
                    else
                        fieldVal.map { toIdOnlyMap(it!!) }
                else if (expand?.containsKey(propName) == true
                    || prop.javaField?.getAnnotation(EmbeddedId::class.java) != null
                )
                    convertToSerializable(fieldVal, expand?.get(propName)?.select, expand?.get(propName)?.expand)
                else toIdOnlyMap(fieldVal)
        }

        return serializableMap
    }

    private fun getMemberProperties(clazz: Class<in Any>): List<KProperty1<in Any, *>> {
        return clazz.kotlin.memberProperties.filter { it.getter.visibility == KVisibility.PUBLIC }
    }

    private fun getClazz(inObj: Any) =
        if (inObj is HibernateProxy) inObj.javaClass.superclass as Class<in Any>
        else inObj.javaClass

    private fun isPrimitive(fieldVal: Any?) = (
        fieldVal is String
            || fieldVal is Number
            || fieldVal is Date)

    private fun isPropertyIgnoredOrNotInSelect(prop: KProperty1<in Any, *>, select: Set<String>?): Boolean {
        return prop.hasAnnotation<JsonIgnore>() || select?.contains(prop.name) != true
    }

    private fun toIdOnlyMap(inObj: Any): Map<String, Any?> {
        val clazz = getClazz(inObj)
        val idProp = clazz.kotlin.memberProperties.find { p ->
            p.javaField?.getAnnotation(Id::class.java) != null
        }
            ?: throw RuntimeException("Object does not have any member with @Id annotation")

        val idVal = idProp.get(inObj)
        return mapOf(idProp.name to idVal)
    }

}
