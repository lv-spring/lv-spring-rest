package com.lv_spring.data.rest.jpa

import com.lv_spring.data.rest.jpa.expand.ExpandBaseVisitor
import com.lv_spring.data.rest.jpa.expand.ExpandParser


class ExpandVisitorImpl : ExpandBaseVisitor<Map<String, Expand?>>() {

    override fun visitExpand(ctx: ExpandParser.ExpandContext?): Map<String, Expand?>? =
        ctx?.expandItem()?.fold(mapOf()) { r, it ->
            r + visit(it)
        }

    override fun visitExpandOptionalSelectExpand(ctx: ExpandParser.ExpandOptionalSelectExpandContext?): Map<String, Expand?> =
        visitExpandSelectContexts(ctx?.PROPERTY()?.text!!, ctx.expandEq()?.expand(), ctx.selectEq().select())

    override fun visitExpandOptionalExpandSelect(ctx: ExpandParser.ExpandOptionalExpandSelectContext?): Map<String, Expand?> =
        visitExpandSelectContexts(ctx?.PROPERTY()?.text!!, ctx.expandEq().expand(), ctx.selectEq().select())

    override fun visitExpandExpandSelect(ctx: ExpandParser.ExpandExpandSelectContext?): Map<String, Expand?> =
        visitExpandSelectContexts(ctx?.PROPERTY()?.text!!, ctx.expand(), ctx.select())

    private fun visitExpandSelectContexts(
            propertyName: String,
            expand: ExpandParser.ExpandContext?,
            select: ExpandParser.SelectContext?
    ): Map<String, Expand?> = mapOf<String, Expand?>(
        propertyName to
                Expand(
                    visitExpand(expand),
                    select?.PROPERTY()?.map { it.text }?.toSet()
                )
    )

    override fun visitExpandPropertyPath(ctx: ExpandParser.ExpandPropertyPathContext?): Map<String, Expand?>? =
        ctx?.PROPERTY()?.reversed()?.fold(null) { r: Map<String, Expand?>?, it ->
            if(r == null) mapOf(it.text to null)
            else mapOf(it.text to Expand(r))
        }
}
