package lv_spring.lvspringdatarestjpa

import com.fasterxml.jackson.databind.ObjectMapper
import com.lv_spring.data.rest.jpa.Expand
import com.lv_spring.data.rest.jpa.ExpandVisitorImpl
import com.lv_spring.data.rest.jpa.expand.ExpandLexer
import com.lv_spring.data.rest.jpa.expand.ExpandParser
import org.antlr.v4.runtime.CharStreams
import org.antlr.v4.runtime.CommonTokenStream
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.skyscreamer.jsonassert.JSONAssert

internal class ExpandVisitorImplTest {

    @Test
    fun `test parse 'prop1'`() {
        val result = parse("prop1")
        assertEquals(result, mapOf("prop1" to null))
    }

    @Test
    fun `test parse 'prop1,prop2'`() {
        val result = parse("prop1,prop2")
        assertEquals(result, mapOf("prop1" to null, "prop2" to null))
    }

    @Test
    fun `test parse 'prop1 prop1_1'`() {
        val result = parse("prop1.prop1_1")
        val jsonResult = ObjectMapper().writeValueAsString(result)
        JSONAssert.assertEquals(
            """{"prop1":{
             "expand":{"prop1_1":null},
             "select":null
        }}""".trimMargin(), jsonResult, false
        )
    }

    @Test
    fun `test parse 'prop1 prop1_1 prop1_1_1'`() {
        val result = parse("prop1.prop1_1.prop1_1_1")
        val jsonResult = ObjectMapper().writeValueAsString(result)
        JSONAssert.assertEquals(
            """{"prop1":{
                 "expand":{"prop1_1":{"expand":{"prop1_1_1":null},
                 "select":null}},
                 "select":null
            }}""",
            jsonResult,
            false
        )
    }

    @Test
    fun `test parse 'prop1 prop1_1 prop1_1_1 prop1_1_1_1'`() {
        val result = parse("prop1.prop1_1.prop1_1_1.prop1_1_1_1")
        val jsonResult = ObjectMapper().writeValueAsString(result)
        JSONAssert.assertEquals(
            """{
              "prop1":{
                "expand":{
                  "prop1_1":{
                    "expand":{
                      "prop1_1_1":{
                        "expand":{"prop1_1_1_1":null},
                        "select":null
                      }
                    },
                    "select":null
                  }
                },
              "select":null
              }
            }""",
            jsonResult,
            false
        )
    }

    @Test
    fun `test parse 'prop1(prop1_1)'`() {
        val result = parse("prop1(prop1_1)")
        val jsonResult = ObjectMapper().writeValueAsString(result)
        assertEquals(jsonResult, """{"prop1":{"expand":{"prop1_1":null},"select":null}}""")
    }

    @Test
    fun `test parse 'prop1(prop1_1,prop1_2)'`() {
        val result = parse("prop1(prop1_1,prop1_2)")
        val jsonResult = ObjectMapper().writeValueAsString(result)
        JSONAssert.assertEquals(
            """{"prop1":{
             "expand":{"prop1_1":null,"prop1_2":null},
             "select":null
            }}""", jsonResult, false
        )
    }

//    @Test
//    fun `test parse 'prop1 prop1_1(prop1_1_1)'`() {
//        val result = parse("prop1.prop1_1(prop1_1_1)")
//        val jsonResult = ObjectMapper().writeValueAsString(result)
//        assertEquals(jsonResult, "{\"prop1\":{" +
//                "\"expand\":{\"prop1_1\":{" +
//                    "\"expand\":{\"prop1_1_1\":null}," +
//                    "\"select\":null}}," +
//                "\"select\":null}}")
//    }

    @Test
    fun `test parse 'prop1(prop1_1 prop1_1_1)'`() {
        val result = parse("prop1(prop1_1.prop1_1_1)")
        val jsonResult = ObjectMapper().writeValueAsString(result)
        assertEquals(
            jsonResult,
            """{"prop1":{"expand":{"prop1_1":{"expand":{"prop1_1_1":null},"select":null}},"select":null}}"""
        )
    }

    @Test
    fun `test parse 'prop1(prop1_1(prop1_1_1))'`() {
        val result = parse("prop1(prop1_1(prop1_1_1))")
        val jsonResult = ObjectMapper().writeValueAsString(result)
        assertEquals(
            jsonResult,
            """{"prop1":{"expand":{"prop1_1":{"expand":{"prop1_1_1":null},"select":null}},"select":null}}"""
        )
    }

    @Test
    fun `test parse 'prop1(prop1_1 prop1_1)'`() {
        val result = parse("prop1(prop1_1;prop1_1,prop1_2)")
        val jsonResult = ObjectMapper().writeValueAsString(result)
        JSONAssert.assertEquals(
            """{"prop1":{
            "expand":{"prop1_1":null},
            "select":["prop1_1","prop1_2"]
        }}""", jsonResult, false
        )
    }

    @Test
    fun `test parse 'prop1($select=prop1_1,prop1_2')`() {
        val result = parse("prop1(\$select=prop1_1,prop1_2)")
        val jsonResult = ObjectMapper().writeValueAsString(result)
        assertEquals(
            jsonResult,
            """{"prop1":{"expand":null,"select":["prop1_1","prop1_2"]}}"""
        )
    }

    @Test
    fun `test parse 'prop1($select=prop1_1,prop1_2 $expand=prop1_1,prop1_2)'`() {
        val result = parse("prop1(\$select=prop1_1,prop1_2;\$expand=prop1_1,prop1_2)")
        val jsonResult = ObjectMapper().writeValueAsString(result)
        assertEquals(
            jsonResult,
            """{"prop1":{"expand":{"prop1_1":null,"prop1_2":null},"select":["prop1_1","prop1_2"]}}"""
        )
    }

    @Test
    fun `test parse 'prop1($expand=prop1_1,prop1_2 $select=prop1_1,prop1_2)'`() {
        val result = parse("prop1(\$expand=prop1_1,prop1_2;\$select=prop1_1,prop1_2)")
        val jsonResult = ObjectMapper().writeValueAsString(result)
        assertEquals(
            jsonResult,
            """{"prop1":{"expand":{"prop1_1":null,"prop1_2":null},"select":["prop1_1","prop1_2"]}}"""
        )
    }

    private fun parse(input: String): Map<String, Expand?>? {
        val lexer = ExpandLexer(CharStreams.fromString(input))
        val tokens = CommonTokenStream(lexer)
        val parser = ExpandParser(tokens).expand()

        return ExpandVisitorImpl().visit(parser)
    }
}
