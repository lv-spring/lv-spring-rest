# LV Spring Rest

![pipeline](https://gitlab.com/lv-spring/lv-spring-rest/badges/master/pipeline.svg)


This library contains `AbstractRestController` which could be extended by Rest APIs that do CRUD operations
