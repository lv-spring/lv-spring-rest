Feature: Authors
  Users should be able to submit GET and POST requests to Authors web API

Scenario: GET Authors
  When GET '/authors?size=0', the server should respond with status 200 and the json response should be
    """
    [
      {
        "firstName": "AuthorFirstName1",
        "lastName": "AuthorLastName1",
        "id": 1
      },
      {
        "firstName": "AuthorFirstName2",
        "lastName": "AuthorLastName2",
        "id": 2
      }
    ]
    """

Scenario: GET Authors sorted DESC
  When GET '/authors?sortBy=id desc&size=0', the server should respond with status 200 and the json response should be
    """
    [
      {
        "firstName": "AuthorFirstName2",
        "lastName": "AuthorLastName2",
        "id": 2
      },
      {
        "firstName": "AuthorFirstName1",
        "lastName": "AuthorLastName1",
        "id": 1
      }
    ]
    """

Scenario: Get Authors with expand books
  When GET '/authors?$expand=books&size=0', the server should respond with status 200 and the json response should be
    """
    [
      {
        "books": [
          {
            "author": {
              "id": 1
            },
            "title": "Book 1",
            "id": 1
          },
          {
            "author": {
              "id": 1
            },
            "title": "Book 2",
            "id": 2
          }
        ],
        "firstName": "AuthorFirstName1",
        "lastName": "AuthorLastName1",
        "id": 1
      },
      {
        "books": [
          {
            "author": {
              "id": 2
            },
            "title": "Book 3",
            "id": 3
          },
          {
            "author": {
              "id": 2
            },
            "title": "Book 4",
            "id": 4
          }
        ],
        "firstName": "AuthorFirstName2",
        "lastName": "AuthorLastName2",
        "id": 2
      }
    ]
"""

Scenario: Get Author 1
  When GET '/authors/1', the server should respond with status 200 and the json response should be
    """
    {
      "firstName": "AuthorFirstName1",
      "lastName": "AuthorLastName1",
      "id": 1
    }
    """

Scenario: Get Author 1 expand books
  When GET '/authors/1?$expand=books', the server should respond with status 200 and the json response should be
    """
    {
      "books": [
        {
          "author": {
            "id": 1
          },
          "title": "Book 1",
          "id": 1
        },
        {
          "author": {
            "id": 1
          },
          "title": "Book 2",
          "id": 2
        }
      ],
      "firstName": "AuthorFirstName1",
      "lastName": "AuthorLastName1",
      "id": 1
    }
    """

Scenario: Get Author 3
  When GET '/authors/3', the server should respond with status 404 and the json response should be
  """
  """

Scenario: Create Author 3
  When POST '/authors', with content: '{"firstName": "AuthorFirstName3", "lastName": "AuthorLastName3"}', the server should respond with status 201 and the json response should be
  """
  {
    "firstName": "AuthorFirstName3",
    "lastName": "AuthorLastName3",
    "id": 3
  }
  """

Scenario: Get Author 3 After created
  When GET '/authors/3', the server should respond with status 200 and the json response should be
  """
  {
    "firstName": "AuthorFirstName3",
    "lastName": "AuthorLastName3",
    "id": 3
  }
  """

Scenario: Update Author 3
  When PUT '/authors', with content: '{"firstName": "AuthorFirstName3Updated", "lastName": "AuthorLastName3Updated","id": 3}', the server should respond with status 200 and the json response should be
  """
  {
    "firstName": "AuthorFirstName3Updated",
    "lastName": "AuthorLastName3Updated",
    "id": 3
  }
  """


Scenario: Get Author 3 After Updated
  When GET '/authors/3', the server should respond with status 200 and the json response should be
"""
{
  "firstName": "AuthorFirstName3Updated",
  "lastName": "AuthorLastName3Updated",
  "id": 3
}
"""

Scenario: Delete Author 3
  When DELETE '/authors/3', the server should respond with status 200 and the json response should be
  """
  """
