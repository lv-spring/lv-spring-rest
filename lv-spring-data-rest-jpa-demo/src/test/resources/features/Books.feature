Feature: Books
  Users should be able to submit GET and POST requests to Books web API

Scenario: GET Books
  When GET '/books?size=0', the server should respond with status 200 and the json response should be
    """
    [
      {
        "title": "Book 1",
        "id": 1
      },
      {
        "title": "Book 2",
        "id": 2
      },
      {
        "title": "Book 3",
        "id": 3
      },
      {
        "title": "Book 4",
        "id": 4
      }
    ]
    """

Scenario: GET Books sorted DESC
  When GET '/books?sortBy=id desc&size=0', the server should respond with status 200 and the json response should be
    """
    [
      {
        "title": "Book 4",
        "id": 4
      },
      {
        "title": "Book 3",
        "id": 3
      },
      {
        "title": "Book 2",
        "id": 2
      },
      {
        "title": "Book 1",
        "id": 1
      }
    ]
    """

Scenario: Get Books with expand authors
  When GET '/books?$expand=author&size=0', the server should respond with status 200 and the json response should be
    """
    [
      {
        "author": {
          "firstName": "AuthorFirstName1",
          "lastName": "AuthorLastName1",
          "id": 1
        },
        "title": "Book 1",
        "id": 1
      },
      {
        "author": {
          "firstName": "AuthorFirstName1",
          "lastName": "AuthorLastName1",
          "id": 1
        },
        "title": "Book 2",
        "id": 2
      },
      {
        "author": {
          "firstName": "AuthorFirstName2",
          "lastName": "AuthorLastName2",
          "id": 2
        },
        "title": "Book 3",
        "id": 3
      },
      {
        "author": {
          "firstName": "AuthorFirstName2",
          "lastName": "AuthorLastName2",
          "id": 2
        },
        "title": "Book 4",
        "id": 4
      }
    ]
  """

Scenario: Get Book 1
  When GET '/books/1', the server should respond with status 200 and the json response should be
    """
    {
      "title":"Book 1",
      "id":1
    }
    """

Scenario: Get Book 1 expand books
  When GET '/books/1?$expand=author', the server should respond with status 200 and the json response should be
    """
    {"author":{"firstName":"AuthorFirstName1","lastName":"AuthorLastName1","id":1},"title":"Book 1","id":1}
    """

Scenario: Get Book 8
  When GET '/books/8', the server should respond with status 404 and the json response should be
  """
  """

Scenario: Create Book 5
  When POST '/books?$expand=author($select=id,firstName,lastName)', with content: '{"title":"Book 5","author": {"id": 1}}', the server should respond with status 201 and the json response should be
  """
  {"author":{"firstName":"AuthorFirstName1","lastName":"AuthorLastName1","id":1},"title":"Book 5","id":5}
  """

Scenario: Get Book 3 After created
  When GET '/books/3', the server should respond with status 200 and the json response should be
  """
  {"title":"Book 3","id":3}
  """

Scenario: Update Book 3
  When PUT '/books', with content: '{"title":"Book 3 Updated","id":3}', the server should respond with status 200 and the json response should be
  """
  {"title":"Book 3 Updated","id":3}
  """


Scenario: Get Book 3 After Updated
  When GET '/books/3', the server should respond with status 200 and the json response should be
"""
{"title":"Book 3 Updated","id":3}
"""

Scenario: Delete Book 3
  When DELETE '/books/3', the server should respond with status 200 and the json response should be
  """
  """
