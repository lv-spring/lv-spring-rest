package com.lv_spring.data.rest.jpa.demo.controllers

import com.lv_spring.data.rest.jpa.AbstractRestController
import com.lv_spring.data.rest.jpa.demo.models.Book
import com.lv_spring.data.rest.jpa.demo.repositories.AuthorRepository
import com.lv_spring.data.rest.jpa.demo.repositories.BookRepository
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.data.jpa.domain.Specification
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/books")
@Tag(name = "Books")
class BooksController(repository: BookRepository, private val authorRepository: AuthorRepository)
    : AbstractRestController<Book, Long>(repository) {

    @GetMapping(params = ["search"])    // we add params search to avoid mapping conflicts with abstract method
    fun findAllCustom(
            @RequestParam(defaultValue = "\${lv_spring.data.rest.jpa.page.size:20}") size: Int,
            @RequestParam(defaultValue = "\${lv_spring.data.rest.jpa.page.number:0}") page: Int,
            @RequestParam(defaultValue = "") sortBy: Set<String>,
            @RequestParam(name = "\$expand", defaultValue = "") expand: String,
            @RequestParam(name = "\$select", defaultValue = "") select: String,
            @RequestParam(defaultValue = "") search: String,
    ): Any? {
        val query =
                if (search.isNotEmpty())
                    Specification<Book> {root, _, cb -> cb.like(root.get("title"), "%$search%")}
                else null
        return super.findAllByQuery(size, page, sortBy, query)
    }

    override fun save(entity: Book): Book {
        if (entity.author != null)
            entity.author = authorRepository.getReferenceById(entity.author!!.id!!)

        return repository.save(entity)
    }
}
