package com.lv_spring.data.rest.jpa.demo.models

import org.hibernate.annotations.Fetch
import org.hibernate.annotations.FetchMode
import jakarta.persistence.*

@Entity
@Table(name = "books")
open class Book(
    open var title: String?,
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    @Fetch(FetchMode.SELECT)
    open var author: Author?
) : AbstractEntity()
