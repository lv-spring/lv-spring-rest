package com.lv_spring.data.rest.jpa.demo.models

import jakarta.persistence.Entity
import jakarta.persistence.OneToMany
import jakarta.persistence.Table

@Entity
@Table(name = "authors")
open class Author(
    open var firstName: String?,
    open var lastName: String?,
    @OneToMany(mappedBy = "author")
    open var books: MutableList<Book> = mutableListOf()
) : AbstractEntity() {
}
